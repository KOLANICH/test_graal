Section: misc
Priority: optional
Homepage: https://github.com/oracle/graal/releases
Standards-Version: 3.9.2
Package: graalvm
Version: 1.0-RC11
Maintainer: KOLANICH
Provides: default-jre, default-jre-headless, openjdk-12-jre, openjdk-12-jre-headless, openjdk-11-jre, openjdk-11-jre-headless, openjdk-10-jre, openjdk-10-jre-headless, openjdk-9-jre, openjdk-9-jre-headless, openjdk-8-jre, openjdk-8-jre-headless, openjdk-7-jre, openjdk-7-jre-headless, openjdk-6-jre, openjdk-6-jre-headless
Architecture: amd64
Copyright: /opt/graal/LICENSE
Readme: /opt/graal/GRAALVM-README.md
Description: GraalVM from GitHub releases








