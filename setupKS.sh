#!/usr/bin/env bash
echo "TRAVIS is $TRAVIS"
if [ "$TRAVIS" = "true" ]; then
  SUDO="sudo";
else
  SUDO="";
fi;
echo "SUDO is $SUDO";
$SUDO apt-get update

if ! $($SUDO apt-get -y install gnupg-curl); then
  $SUDO apt-get -y install gnupg1-curl
fi;
$SUDO apt-get install -y apt-transport-https

echo "deb https://dl.bintray.com/kaitai-io/debian_unstable jessie main" | $SUDO tee /etc/apt/sources.list.d/kaitai.list
$SUDO apt-key adv --keyserver hkps://keyserver.ubuntu.com --recv 8756C4F765C9AC3CB6B85D62379CE192D401AB61
$SUDO apt-get update
$SUDO apt-get install -y kaitai-struct-compiler

if ! $(java -version); then
  $SUDO apt-get install -y default-jre-headless;
fi;

#git clone --depth=1 https://github.com/kaitai-io/kaitai_struct_formats.git "$KAITAI_STRUCT_ROOT/formats"
ksc --help
pip3 install --upgrade git+https://github.com/kaitai-io/kaitai_struct_python_runtime.git
