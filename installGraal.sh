#!/usr/bin/env bash
export GRAAL_VERSION=1.0.0-rc11
export GRAAL_URI="https://github.com/oracle/graal/releases/download/vm-${GRAAL_VERSION}/graalvm-ce-${GRAAL_VERSION}-linux-amd64.tar.gz"
export GRAAL_UNPACKED_DIR="./graalvm-ce-${GRAAL_VERSION}"
export JAVA_HOME="/opt/graal"
aria2c --continue=true --enable-mmap=true --optimize-concurrent-downloads=true -j 16 -x 16 --file-allocation=falloc -o ./graal.txz $GRAAL_URI
tar --restrict -xzf ./graal.txz
rm ./graal.txz
ls -l ./
ls -l $GRAAL_UNPACKED_DIR
mv $GRAAL_UNPACKED_DIR $JAVA_HOME
ls -l $JAVA_HOME/jre
ls -l $JAVA_HOME/jre/languages
rm -rf $JAVA_HOME/jre/languages/js
find $JAVA_HOME -iname "src.zip" -exec rm {} \;
find $JAVA_HOME -iname "*.src.zip" -exec rm {} \;
rm -rf $JAVA_HOME/lib/visualvm
rm -rf $JAVA_HOME/sample
rm -rf $JAVA_HOME/man
rm -rf $JAVA_HOME/lib/visualvm

export PATH=$PATH:$JAVA_HOME/bin
java -version
gu available
gu install --no-progress -o python

apt install -y equivs
equivs-build ./graalvm.ctl
apt remove -y --purge equivs
ls -l
dpkg -i ./graalvm_*.deb
